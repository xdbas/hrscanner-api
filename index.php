<?php
error_reporting(-1);
ini_set("display_errors", "On");

chdir('.');
require 'vendor/autoload.php';

require_once 'Google/Client.php';
require_once 'Google/Service/Calendar.php';
require_once 'client.php';
require_once 'capsule.php';


$client = Client::googleService();
$capsule = CapsuleDB::getInstance();

$cal = new Google_Service_Calendar($client);

$calendar = $cal->calendars->get("bj637jqngsjgeve1l0b28jfi3s@group.calendar.google.com");

$time_pre = date("Y-m-d");
$timeMin = $time_pre . 'T08:30:00Z';
$timeMax = $time_pre . 'T22:00:00Z';


if (isset($_GET['studentCode'], $_GET['studentClass'], $_GET['classCode']) === false) {
    header('HTTP/1.1 400 Bad Request');
    echo json_encode(
        array(
            'errors' => array(
                'checkedIn' => false,
                'message' => 'Not all GET parameters are accepted',
                'exception' => array(),
                'error' => 400,
            )
        )
    );
    exit;
}

foreach ($cal->events->listEvents(
             "bj637jqngsjgeve1l0b28jfi3s@group.calendar.google.com",
             array(
                 'timeMin' => $timeMin,
                 'timeMax' => $timeMax,
             )
         )->getItems() as $event) {


    $name = $event->getDescription();
    $desc = $event->getSummary();
    $summary_match = false;
    if (isset($_GET['classCode'])) {
        $summary = $event->getSummary();

        $summary_match = strpos(strtolower($summary), strtolower($_GET['classCode'])) !== false;
    }

    $begin = strtotime($event->getStart()->dateTime);
    $end = strtotime($event->getEnd()->dateTime);

    if ($capsule::table('classes')
            ->where('classCode', '=', $name)
            ->where('timeStart', '=', $begin)
            ->where('timeEnd', '=', $end)->count()
        == 0
    ) {
        $class = new Classes();
        $class->ClassCode = $name;
        $class->timeStart = $begin;
        $class->timeEnd = $end;
        $class->save();
    }

    $class_now = $capsule::table('classes')
        ->where('classCode', '=', $name)
        ->where('timeStart', '=', $begin)
        ->where('timeEnd', '=', $end)->first();

    if ($class_now == null) {
        header('HTTP/1.1 500 Internal Server Error');
        echo json_encode(
            array(
                'checkedIn' => false,
                'errors' => array(
                    'message' => 'Something went wrong with the database',
                    'exception' => array(),
                    'error' => 400,
                )
            )
        );
        exit;
    }

    if ($capsule::table('students')
            ->where('class_id', '=', $class_now['id'])
            ->where('studentCode', '=', $_GET['studentCode'])
            ->count() == 0
    ) {
        //Class found / adding student to the list
        $student = new Student();
        $student->class_id = $class_now['id'];
        $student->studentCode = $_GET['studentCode'];
        $student->save();
    }


    if (time() > $begin && time() < $end) {
        $result = json_encode(
            array(
                'checkedIn' => true,
                'classMatch' => $summary_match
            )
        );
        break;
    }
}

/**Write to export file**/
$export_file = $class_now['classCode'] . '_' . $class_now['timeStart'] . '_' . $class_now['timeEnd'] . '.csv';

$students = Student::where('class_id', '=', $class_now['id'])->get();

$fp = fopen($export_file, 'w');
foreach ($students as $line) {
    $val = array($line['studentCode'], $line['created_at']);
    fputcsv($fp, $val);
}
fclose($fp);


header('HTTP/1.1 200 OK');
header('Content-Type: application/json; charset=UTF-8');
echo $result;

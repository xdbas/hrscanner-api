<?php

class Client {

    public static $service;


    public static function googleService()
    {
        return static::$service !== null
            ? static::$service
            : (static::$service = static::createService());
    }

    private static function createService()
    {
        $client = new Google_Client();
        $client->setApplicationName("API Project");
        $client->setDeveloperKey("AIzaSyDuC3zX6g5qGuGHJbbXHYjMLybPnObImY4");

//        $service = new Google_Service_Books($client);


        if (isset($_SESSION['service_token'])) {
            $client->setAccessToken($_SESSION['service_token']);
        }

        $key = file_get_contents("cae362b218a0f634656ae553f4183fa48cb1aa41-privatekey.p12");
        $cred = new Google_Auth_AssertionCredentials(
// Replace this with the email address from the client.
            "433174333411-dtol0n0oll528l9andgpjhcmk4rb8eb8@developer.gserviceaccount.com",
            // Replace this with the scopes you are requesting.
            array(
                'https://www.googleapis.com/auth/books',
                'https://www.googleapis.com/auth/calendar',
            ),
            $key
        );
        $client->setAssertionCredentials($cred);


        if ($client->getAuth()->isAccessTokenExpired()) {
            $client->getAuth()->refreshTokenWithAssertion($cred);
        }
        $_SESSION['service_token'] = $client->getAccessToken();

        return $client;
    }

}
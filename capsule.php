<?php
use \Illuminate\Database\Capsule\Manager as Manager;
use \Illuminate\Events\Dispatcher;
use \Illuminate\Container\Container;

class Capsule extends Manager {

    public function __construct(array $options = array())
    {
        parent::__construct();

        $this->addConnection($options);

        $this->setEventDispatcher(new Dispatcher(new Container));
        $this->setAsGlobal();
        $this->bootEloquent();
    }

}

class CapsuleDB extends \Illuminate\Database\Eloquent\Model
{

    public static $CapsuleDB;


    public function  __construct()
    {
        try {
            static::capsule();
        } catch(Exception $e) {

        }
    }

    private static function capsule()
    {
        if (false ===  static::$CapsuleDB instanceof CapsuleDB) {

            static::$CapsuleDB = new Capsule(array(
                'driver'    => 'mysql',
                'host'      => 'localhost',
                'database'  => 'bas_hrscanner',
                'username'  => 'bas',
                'password'  => '',
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
            ));
        }

        return static::$CapsuleDB;
    }



    public static function getInstance()
    {
        return static::capsule();
    }

}

class Classes extends CapsuleDB
{
    protected $table = 'classes';
}

class Student extends CapsuleDB
{
    protected $table = 'students';
}

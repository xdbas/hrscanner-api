<?php
error_reporting(-1);
ini_set("display_errors", "On");

chdir('.');


require_once 'Google/Client.php';
require_once 'Google/Service/Calendar.php';
require_once 'Google/Service/Books.php';
require_once 'Google/Service/Drive.php';


$client = new Google_Client();
$client->setApplicationName("API Project");
$client->setDeveloperKey("AIzaSyDuC3zX6g5qGuGHJbbXHYjMLybPnObImY4");

$service = new Google_Service_Books($client);
//$cal = new Google_Service_Calendar($client);
$drive = new Google_Service_Drive($client);

if (isset($_SESSION['service_token'])) {
    $client->setAccessToken($_SESSION['service_token']);
}

$key = file_get_contents("cae362b218a0f634656ae553f4183fa48cb1aa41-privatekey.p12");
$cred = new Google_Auth_AssertionCredentials(
// Replace this with the email address from the client.
    "433174333411-dtol0n0oll528l9andgpjhcmk4rb8eb8@developer.gserviceaccount.com",
    // Replace this with the scopes you are requesting.
    array(
        'https://www.googleapis.com/auth/books',
        'https://www.googleapis.com/auth/calendar',
        'https://www.googleapis.com/auth/drive',
    ),
    $key
);
$client->setAssertionCredentials($cred);


if ($client->getAuth()->isAccessTokenExpired()) {
    $client->getAuth()->refreshTokenWithAssertion($cred);
}
$_SESSION['service_token'] = $client->getAccessToken();

//$calendar = $cal->calendars->get("bj637jqngsjgeve1l0b28jfi3s@group.calendar.google.com");
//Insert a file
$file = new Google_Service_Drive_DriveFile();
$file->setTitle('My document');
$file->setDescription('A test document');
$file->setMimeType('text/plain');


$permission = new Google_Service_Drive_Permission();
$permission->setRole('reader');
$permission->setType('anyone');
$permission->setValue('basje1@gmail.com');
$permission->setWithLink(true);
$permission->setName('basje1@gmail.com');
$permission->setKind('drive#permission');
$permission->setEmailAddress('basje1@gmail.com');

$file->setUserPermission($permission);
//$file->setShared($permission);
//$file->setOwners($permission);

$data = file_get_contents('document.txt');

$createdFile = $drive->files->insert($file, array(
        'data' => $data,
        'mimeType' => 'text/plain',
        'uploadType' => 'media'
    ));

print_r($createdFile);

